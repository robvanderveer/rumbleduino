/*
 * RumbleDuino
 *
 * Turns sound into tactile feedback.
 *
 * This Sketch takes an analog audio input (L+R), applies a digital low-pass filter and emits
 * the lowest frequencies though a vibration motor. The level at which the effect peaks is
 * adjustable through a potmeter on the 2 remaining analog pins. No other rocket science.
 *
 * A0 --- left audio in
 * A1 --- right audio in
 * A2 --- sensitivity level in
 * 10 --- PWM out left
 * 11 --- PWM out right
 *
 * Add proper flyback and resistors to your mosfets; i'm using IRLB8721's.
 * Add a pulldown resistor and an inline 0.1uF cap to the audio input.
 *
 * http://www.simplicate.info
 * Rob van der Veer, 2015
 */

#include <Filters.h>

#define SERIAL_DEBUG

int PIN_LEFT_RUMBLE = 5;
int PIN_RIGHT_RUMBLE = 6;
int PIN_LEFT_AUDIO = A0;
int PIN_RIGHT_AUDIO = A1;
int PIN_SENSITITIVY = A2;

/* TODO implement clipping feedback */
int PIN_CLIP_LED = 13;

float filterFrequency = 15.0;

FilterOnePole lowpassFilterLeft( LOWPASS, filterFrequency );
FilterOnePole lowpassFilterRight( LOWPASS, filterFrequency );

// the setup routine runs once when you press reset:
void setup() {
#ifdef SERIAL_DEBUG
  Serial.begin(9600);
#endif

  // initialize the digital pin as an output.
  pinMode(PIN_LEFT_RUMBLE, OUTPUT);
  pinMode(PIN_RIGHT_RUMBLE, OUTPUT);
  pinMode(PIN_LEFT_AUDIO, INPUT);
  pinMode(PIN_RIGHT_AUDIO, INPUT);

  analogWrite(PIN_LEFT_RUMBLE, 0);
  analogWrite(PIN_RIGHT_RUMBLE, 0);
}


//level = 0..1
void mapLevel(uint8_t pin, float value, float level)
{
  float threshold = 0.75 * level;

  if(value > threshold)
  {
    float clipped = constrain(value, threshold, level);
    float mapped = map(clipped, threshold, level, 50, 255);
    mapped = constrain(mapped, 0, 255);
    
    analogWrite(pin, mapped);
  }
  else
  {
    analogWrite(pin, 0);
  }
}

// the loop routine runs over and over again forever:
void loop()
{
  //adjust input.
  int sensitivityRaw = analogRead(PIN_SENSITITIVY);
  float sensitivity = (float)sensitivityRaw / 1023.0;
  
  lowpassFilterLeft.input( constrain(0, analogRead( PIN_LEFT_AUDIO ), 511) );   //the constrains are for clipping.
  lowpassFilterRight.input( constrain(0, analogRead( PIN_RIGHT_AUDIO ), 511) );
  float left =  lowpassFilterLeft.output();
  float right =  lowpassFilterRight.output();

  mapLevel(PIN_LEFT_RUMBLE, left * sensitivity, 10);
  mapLevel(PIN_RIGHT_RUMBLE, right * sensitivity, 10);

#ifdef SERIAL_DEBUG
  Serial.print(sensitivity, DEC);
  Serial.print("\t");
  Serial.print(left, DEC);
  Serial.println();
#endif
  delay(1);
}
